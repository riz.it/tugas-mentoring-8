<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>TUGAS 8</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
			text-decoration: none;
		}

		a:hover {
			color: #97310e;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
			min-height: 96px;
		}

		p {
			margin: 0 0 10px;
			padding: 0;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<body>

	<div id="container">
		<h1>PHP SpreadSheet</h1>

		<div id="body">
			<b>Export SpreadSheet</b><br>
			<input style="margin-top: 8px;" type="text" size="33" name="fileName" id="fileName" placeholder="Ketikan nama file">
			<button id="export">Export</button>

			<hr style="margin-top: 15px;">
			<b>Import SpreadSheet</b><br>
			<form action="<?= base_url('welcome/import'); ?>" method="post" enctype="multipart/form-data">
				<input type="file" accept=".xls,.xlsx,.csv" style="margin-top: 8px;" size="30" name="fileImport" id="fileImport">
				<button type="button" style="margin-bottom: 8px;" id="import">Import</button><br>
			</form>
			<hr>
			<b>PREVIEW IMPORT : </b><br>
			<div id="preview-file" style="margin-top: 5px;">
				<?php echo $excel_html; ?>
			</div>
			<br>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
		var customElement = $("<div>", {
			css: {
				border: "5px dashed",
				"font-size": "50px",
				"text-align": "center",
				padding: "50px",
			},
			class: "spinner-border spinner-border-lg text-primary",
			text: "",
		});

		$(document).ready(function() {
			$('#export').click(function() {
				var fileName = $('#fileName').val();
				$.ajax({
					url: '<?= base_url('welcome/export/') ?>' + fileName,
					type: 'GET',
					dataType: 'JSON',
					beforeSend: function() {
						$.LoadingOverlay("show", {
							image: "",
							custom: customElement,
						});
					},
					success: function(data) {
						$.LoadingOverlay("hide");
						if (data.status) {
							$('#fileName').val('');
							toastr["success"]("Berhasil export data. <a style='color: #4abad8;' href='" + data.filePath + "' >Download sekarang!</a>");
						} else {
							toastr["error"]("Gagal mengexport data.");
						}
					}
				});
			});
			$('#import').click(function(e) {
				e.preventDefault();
				var formData = new FormData();
				var fileUpload = $('#fileImport')[0].files[0];

				formData.append('fileImport', fileUpload);

				$.ajax({
					url: '<?= base_url('welcome/import') ?>',
					type: 'POST',
					dataType: 'JSON',
					data: formData,
					processData: false,
					contentType: false,
					beforeSend: function() {
						$.LoadingOverlay("show", {
							image: "",
							custom: customElement,
						});
						$('#preview-file').html('');
					},
					success: function(data) {
						if (data.status) {
							$('#preview-file').html(data.content);
							$('#fileImport').val('');
							$.LoadingOverlay("hide");
							toastr["success"]("Berhasil import data.");

						} else {
							$.LoadingOverlay("hide");
							toastr["error"](data.message);
						}
					},
					error: function(error) {
							$.LoadingOverlay("hide");
						toastr["error"](error.responseText);
					}
				});
			});

		});
	</script>
</body>

</html>