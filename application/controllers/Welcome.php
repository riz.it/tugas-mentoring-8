<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'form', 'download']);
		$this->load->library('form_validation');

	}
	public function index()
	{
		

        // Data untuk dikirimkan ke view
        $data['excel_html'] = $this->readData();

		$this->load->view('welcome_message', $data);
	}

	public function export($fileName)
	{
		$spreadsheet = new Spreadsheet();

		$sheet = $spreadsheet->getActiveSheet();

		$field = array(
			"A" => 'Nama Lengkap',
			"B" => 'Fakultas',
			"C" => 'Prodi',
			"D" => 'Telepon',
			"E" => 'Jenis Kelamin',
			"F" => 'Alamat',
			"G" => 'Tanggal Lahir'
		);

		$data = [
			"A" => 'Mochamad Rizal Fachrudin',
			"B" => 'Fakultas Teknologi Informasi',
			"C" => 'Sistem Informasi',
			"D" => '08123456789',
			"E" => 'Laki-laki',
			"F" => 'Jl. Raya Kedung Halang No. 1',
			"G" => '1998-01-01'
		];


		foreach ($field as $column => $value) {
			$cell = $column . 1;
			$sheet->setCellValue($cell, $value);
		}

		foreach ($data as $column => $value) {
			$cell = $column . 2;
			$sheet->setCellValue($cell, $value);
		}

		$savePath = 'public/export/';

		if (!is_dir($savePath)) {
			mkdir($savePath, 0777, true);
		}

		$writer = new Xlsx($spreadsheet);

		$saveFileName = $fileName . '.xlsx';
		$saveFullPath = $savePath . $saveFileName;
		$writer->save($saveFullPath);
		$status = [
			"status" => true,
			"filePath" => $saveFullPath,
		];
		echo json_encode($status);
	}

	public function import()
	{
		$config['upload_path'] = './public/import/';
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 10000;


		$this->load->library('upload');
		$this->upload->initialize($config);

        if (!$this->upload->do_upload('fileImport')) {
            $error = $this->upload->display_errors();
			$response = [
				"status" => false,
				"message" => $error
			];
			echo json_encode($response);
        } else {

			
			$data = $this->upload->data();
			
			$objPHPExcel = null;

			if ($data['file_ext'] == '.xls') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
			} else if ($data['file_ext'] == '.xlsx') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
			} else if ($data['file_ext'] == '.csv') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
			}

			if ($objPHPExcel == null) {
				$response = [
					"status" => false,
					"message" => "invalid file extension"
				];
				echo json_encode($response);
				return false;
			}

			
			$objPHPExcel = $objPHPExcel->load($data['full_path']);
			$sheet = $objPHPExcel->getActiveSheet();

			
			$highestRow = $sheet->getHighestRow();

			
			$highestColumn = $sheet->getHighestColumn();

		
			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

			if ($sheet->getCell('A1')->getValue() != 'Nama Lengkap') {
				echo "Format tidak valid, cell A1 harus berisi Nama Lengkap";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('B1')->getValue() != 'Fakultas') {
				echo "Format tidak valid, cell B1 harus berisi Fakultas";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('C1')->getValue() != 'Prodi') {
				echo "Format tidak valid, cell C1 harus berisi Prodi";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('D1')->getValue() != 'Telepon') {
				echo "Format tidak valid, cell D1 harus berisi Telepon";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('E1')->getValue() != 'Jenis Kelamin') {
				echo "Format tidak valid, cell E1 harus berisi Jenis kelamin";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('F1')->getValue() != 'Alamat') {
				echo "Format tidak valid, cell F1 harus berisi Alamat";
				unlink($data['full_path']);
				return;
			}

			if ($sheet->getCell('G1')->getValue() != 'Tanggal Lahir') {
				echo "Format tidak valid, cell G1 harus berisi Tanggal Lahir";
				unlink($data['full_path']);
				return;
			}

			// save file as 'public/data.xlsx'
			$writer = new Xlsx($objPHPExcel);
			$writer->save('public/import/import-data.xlsx');

			
			if(unlink($data['full_path'])){
				$response = [
					"status" => true,
					"message" => "success",
					"content" => $this->readData()
				];
				echo json_encode($response);
			}
        }
	}

	private function readData()
	{
		// Lokasi file Excel yang akan di-preview
        $excelFile = 'public/import/import-data.xlsx';
        // Membaca file Excel
        $spreadsheet = IOFactory::load($excelFile);
        $sheet = $spreadsheet->getActiveSheet();

        // Menginisialisasi HTML
        $html = '<table style="border-collapse: collapse; border: 1px solid black; padding: 5px;">';

        // Mengambil semua baris dan sel dalam lembar kerja
        foreach ($sheet->getRowIterator() as $row) {
            $html .= '<tr>';
            foreach ($row->getCellIterator() as $cell) {
                $html .= '<td style="border: 1px solid black; padding: 5px;">' . $cell->getValue() . '</td>';
            }
            $html .= '</tr>';
        }

        $html .= '</table>';
		return $html;
	}
}
